//
//  LoadData.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import RealmSwift

extension ProductsViewController{
    
    
    
// funcion para cargar la data mediante plist
    func loadBd(){
        let dictionary = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "ItemList", ofType: "plist")!);
        let productos = dictionary?["Productos"] as! [NSDictionary] // se parsea a dictionary
        
        // se traduce el diccionario a la clase product
        for producto in productos {
            if let _ = producto["name"], let _ = producto["price"], let _ = producto["stock"]{
                insertProductBd(producto) // se valida de que la data en el plist exista y se inserta en la bd
            }
        }
    }
    
    // se actualiza la bd con la info del plist
    func insertProductBd(_ dic: NSDictionary){ // se inserta cada dato en la bedd
        
        
        try! realm.write(){
        
            let productItem = Product()
            // Asignar valores.
            productItem.name = dic["name"] as! String
            productItem.stock = dic["stock"] as! Int
            productItem.price = dic["price"] as! Float
            realm.add(productItem)
        }
       
    }
    
    //se recupera la info de la bd
    func recoverProduct() {
        
        stockShop.removeAll() //se limpia la cache de la app
        let Products: Results<Product> = { realm.objects(Product.self) }()
        
       if Products.count > 0 {
        
            for  result in Products {
                _ = result.resetStock()
                stockShop.append(result) //se castea al modelo y guarda en la cache
            }
        }
    }
    
    // momento cuando se vende el producto y desaparece el objeto del stock
    func sellProduct(){
        
        let predicate = NSPredicate(format: "onSale > 0"); // se filtran todos los elementos que estan en caja
    
        let searchResults: Results<Product> = { realm.objects(Product.self).filter(predicate) }()
        for result in searchResults {
            _ = result.sellStock() //se vende el producto
        }
    }

    
}
