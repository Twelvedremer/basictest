//
//  Product.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import RealmSwift

public class Product: Object {
    dynamic var name:String = ""
    dynamic var price:Float = 0.0
    dynamic var stock:Int = 0
    dynamic var onSale:Int = 0
    
    // valida si existen cosas en caja
    func isCash() -> Bool {
        if self.onSale > 0 {
            return true
        }
        return false
    }
    
    //valida si hay cosas en el stock
    func isStock() -> Bool {
        if self.stock > 0 {
            return true
        }
        return false
    }
    
    // retorna el costo total entre el precio y la cantidad de productoss
    func totalBuy() -> Float {
        return self.price * Float(self.onSale)
    }
    
    // agrega cosas a la caja
    func addCash() -> Bool {
        if isStock() {
            let realm = try! Realm()
            try! realm.write {
                self.stock-=1
                self.onSale+=1
            }
            return true
        }
        return false
    }
    
    //agrega cosas al stock
    func addStock() -> Bool {
        if isCash() {
            let realm = try! Realm()
            try! realm.write {
                self.stock+=1
                self.onSale-=1
            }
            return true
        }
        return false
    }
    
    // devuelve todos los elementos al stock
    func resetStock() -> Bool{
        if isCash() {
            let realm = try! Realm()
            try! realm.write {
                self.stock += self.onSale
                self.onSale = 0
            }
            return true
        }
        return false
    }
    
    func sellStock() -> Bool{
        if isCash() {
            let realm = try! Realm()
            try! realm.write {
                self.onSale = 0
            }
            return true
        }
        return false
    }
}
