//
//  onSaleViewController.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol onSaleViewProtocol: class {
    func buyAll() // evento cuando se compra todos los elementos
    func returnAll(row:Int) // evento cuando se devuelve todos los items
    
}

class OnSaleViewController: UIViewController {
    
    let idCell = "onSaleCell"
    var onSaleShop: [(Product,Int)] = [] // se tiene una lista aparte de los objetos en caja en formato tupla (producto, indice en la lista del stock)
    
    @IBOutlet weak var tablaRefresh: UITableView!
    weak var cellDelegate : OnSaleCellProtocol?
    weak var viewDelegate : onSaleViewProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buyAllItems(){
        if onSaleShop.count > 0{ // si hay elementos en caja, se manda una alerta para ver si quiere comprar todo
            alertModal(title: "Confirm", message: "Do you want to continue shopping?", response: { () -> Bool in
                self.viewDelegate?.buyAll()
                self.onSaleShop = [] // si acepta se actualiza la lista local y se refresca
                self.tablaRefresh.reloadData()
                let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let _ = self.navigationController?.popViewController(animated: true)
                }
                return true;
            })
            
        } else{ // si no hay elementos se manda una alerta
            
            alertModal(title: "Warning", message: "You have no items in the cart", cancel: false, response: {() -> Bool in return true})
            
        }
    }
    
    func alertModal(title: String, message:String, cancel:Bool = true, response: @escaping () -> Bool ) {
        let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            _ = response()
        }))
        if cancel {
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        }
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
}


extension OnSaleViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return onSaleShop.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell, for: indexPath) as! OnSaleTableViewCell
        
        
        cell.loadData(name:onSaleShop[indexPath.row].0.name!, count: Int(onSaleShop[indexPath.row].0.onSale),num:onSaleShop[indexPath.row].1,row: indexPath.row, delegate: self)
        
        return cell
    }
    
    // evento para borrar todos los elementos de caja mediantes slice
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            
            alertModal(title: "Warning", message: "Do you want to delete all items in the cart?", response: {()-> Bool in self.viewDelegate?.returnAll(row: self.onSaleShop[indexPath.row].1)
                self.onSaleShop
                    .remove(at: indexPath.row)
                self.tablaRefresh.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                return true
            })
           
        }
    }
    
    
}

extension OnSaleViewController : OnSaleEventProtocol {
    
    func updateview(num:Int){ //actualiza la vista al eliminar el objeto de la lista de caja
        onSaleShop.remove(at: num)
        tablaRefresh.reloadData()
    }
    
    func returnProduct(num:Int) -> Int { // se retorna un objeto
        return (self.cellDelegate?.returnProduct(num: num))!
    }
    
    func buyAll(){ //se compran todos los elementos
        return (self.viewDelegate?.buyAll())!
    }
    
    func returnAll(row:Int){// se retornan todos los elementos
        return (self.viewDelegate?.returnAll(row: row))!
    }
    
}
