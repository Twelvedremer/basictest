//
//  onSaleTableViewCell.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

protocol OnSaleCellProtocol : class {
     func returnProduct(num:Int) -> Int // evento de cuando se devuelve un producto
}

protocol OnSaleEventProtocol: OnSaleCellProtocol  {
    func updateview(num:Int) // actualiza la vista cuando este queda en 0
}

class OnSaleTableViewCell: UITableViewCell {

    // label de la celda
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    
    // numero de la celda en caja
    var numCell: Int!
    
    // numero de la celda en el stock
    var numRow : Int!
    
    //delegate de la vista de caja
    weak var delegate : OnSaleEventProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // se carga la informacion de la celda
    func loadData(name:String, count :Int, num: Int, row:Int, delegate: OnSaleEventProtocol) {
        self.nameLabel.text = name
        self.countLabel.text = "\(count)"
        self.numCell = num
        self.numRow = row
        self.delegate = delegate
    }
    
    
    // evento que ocurre cuando se da al boton de retornar un producto
    @IBAction func moveToStock(){
        if let state = delegate?.returnProduct(num : numCell){
            if state == 0 {
                self.delegate?.updateview(num: numRow)
            }else {
                self.countLabel.text = "\(state)"
            }
            
        }
    }


}
