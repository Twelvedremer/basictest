//
//  Product+CoreDataProperties.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/9/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import CoreData


public class Product: NSManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product");
    }

    @NSManaged public var name: String?
    @NSManaged public var stock: Int16
    @NSManaged public var onSale: Int16
    @NSManaged public var price: Float
    
    // valida si existen cosas en caja
    func isCash() -> Bool {
        if self.onSale > 0 {
            return true
        }
        return false
    }
    
    //valida si hay cosas en el stock
    func isStock() -> Bool {
        if self.stock > 0 {
            return true
        }
        return false
    }
    
    // retorna el costo total entre el precio y la cantidad de productoss
    func totalBuy() -> Float {
        return self.price * Float(self.onSale)
    }
    
    // agrega cosas a la caja
    func addCash() -> Bool {
        if isStock() {
            self.stock-=1
            self.onSale+=1
            return true
        }
        return false
    }
    
    //agrega cosas al stock
    func addStock() -> Bool {
        if isCash() {
            self.stock+=1
            self.onSale-=1
            return true
        }
        return false
    }
    
    // devuelve todos los elementos al stock
    func resetStock() -> Bool{
        if isCash() {
            self.stock += self.onSale
            self.onSale = 0
            return true
        }
        return false
    }
    
    func sellStock() -> Bool{
        if isCash() {
            self.onSale = 0
            return true
        }
        return false
    }


}
