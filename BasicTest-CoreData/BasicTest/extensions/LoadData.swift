//
//  LoadData.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import CoreData
import UIKit

extension ProductsViewController{
    
    
   
// funcion para cargar la data mediante plist
    func loadBd(){
        let dictionary = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "ItemList", ofType: "plist")!);
        let productos = dictionary?["Productos"] as! [NSDictionary] // se parsea a dictionary
        
        // se traduce el diccionario a la clase product
        for producto in productos {
            if let _ = producto["name"], let _ = producto["price"], let _ = producto["stock"]{
                insertProductBd(producto) // se valida de que la data en el plist exista y se inserta en la bd
            }
        }
    }
    
    // se actualiza la bd con la info del plist
    func insertProductBd(_ dic: NSDictionary){ // se inserta cada dato en la bedd
        
        let context = getContext() // se extrae la informacion del contexto de la bd
        //se crean un puntero a la entidad producto
        let productEntity =  NSEntityDescription.entity(forEntityName: "Product", in: context)
        let productItem = NSManagedObject(entity: productEntity!, insertInto: context)
        
        // Asignar valores.
        productItem.setValue(dic["name"] as! String, forKey: "name")
        productItem.setValue(dic["stock"] as! Float, forKey: "stock")
        productItem.setValue(dic["price"] as! Int, forKey: "price")
        
        do {
                try context.save() //se actualiza la bd
            } catch let error as NSError {
                print("Error al insertar: \(error)")
            }
    }
    
    //se recupera la info de la bd
    func recoverProduct() {
        
        let context = getContext()
        stockShop.removeAll() //se limpia la cache de la app
        let fetchRequest = NSFetchRequest<Product>(entityName: "Product")
        fetchRequest.predicate = NSPredicate(format: "stock > 0") // se filtra todos los elementos que su stock no este vacio
        
        do {
            
            // se realiza la consulta
                let results = try context.fetch(fetchRequest)
    
                if results.count > 0 {
                    for  result in results as [NSManagedObject]{
                        stockShop.append(result as! Product) //se castea al modelo y guarda en la cache
                    }
                }
        }
        catch let error as NSError {
            print("Error al recuperar: \(error)")
        }
        
    }
    
    // momento cuando se vende el producto y desaparece el objeto del stock
    func sellProduct(){
        
        let context = getContext()
        
        let fetchRequest = NSFetchRequest<Product>(entityName: "Product")
        fetchRequest.predicate = NSPredicate(format: "onSale > 0 ") // se filtran todos los elementos que estan en caja
        do {
            let searchResults = try context.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
            for result in searchResults as! [NSManagedObject] {
                let resultCastProduct =  result as! Product
                _ = resultCastProduct.sellStock() //se vende el producto
                do {
                    try context.save() //se actualiza la bd
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
                
            }
        } catch let error as NSError {
            print("Error al editar: \(error)")
        }
    }
    
    
    func getContext () -> NSManagedObjectContext { //obtiene el contexto
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
}
