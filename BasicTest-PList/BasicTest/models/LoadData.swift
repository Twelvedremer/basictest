//
//  LoadData.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation

extension ProductsViewController{
    
// funcion para cargar la data mediante plist
    func loadBd(){
    
        let dictionary = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "ItemList", ofType: "plist")!);
        let productos = dictionary?["Productos"] as! [NSDictionary] // se parsea a dictionary
        
        // se traduce el diccionario a la clase product
        for producto in productos {
            if let Tname = producto["name"], let Tprice = producto["price"], let Tstock = producto["stock"]{
                 stockShop.append(Product(name: Tname as! String, price: Tprice as! Float, stock: Tstock as! Int))
            }
        }
        
    }

}
