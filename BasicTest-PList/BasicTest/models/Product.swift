//
//  Product.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation

class Product {
    var name:String!
    var price:Float!
    var stock:Int = 0
    var onSale:Int = 0
    
    //constructor
    init(name nombre: String, price precio: Float, stock cantidad: Int) {
        self.name = nombre
        self.price = precio
        self.stock = cantidad
    }
    
    // valida si existen cosas en caja
    func isCash() -> Bool {
        if self.onSale > 0 {
            return true
        }
        return false
    }
    
    //valida si hay cosas en el stock
    func isStock() -> Bool {
        if self.stock > 0 {
            return true
        }
        return false
    }
    
    // retorna el costo total entre el precio y la cantidad de productoss
    func totalBuy() -> Float {
        return self.price * Float(self.onSale)
    }
    
    // agrega cosas a la caja
    func addCash() -> Bool {
        if isStock() {
            self.stock-=1
            self.onSale+=1
            return true
        }
        return false
    }
    
    //agrega cosas al stock
    func addStock() -> Bool {
        if isCash() {
            self.stock+=1
            self.onSale-=1
            return true
        }
        return false
    }
    
    // devuelve todos los elementos al stock
    func resetStock() -> Bool{
        if isCash() {
            self.stock += self.onSale
            self.onSale = 0
            return true
        }
        return false
    }
    
    func sellStock() -> Bool{
        if isCash() {
            self.onSale = 0
            return true
        }
        return false
    }
}
