//
//  StockTableViewCell.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit


protocol StockCellProtocol : class {
    func buyProduct(num:Int) -> Int // evento de cuando se compra un producto
    
}

class StockTableViewCell: UITableViewCell {

    
    // referencias a los label
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var stockLabel: UILabel!
    
    //numero de la celda
    var numCell: Int!
    //delegate de la celda
    weak var delegate : StockCellProtocol?
  
    //boton de comprar
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // se carga la informacion de la celda
    func cargarLabel(delegate:StockCellProtocol, product:Product, NumRow num:Int) {
        self.button.isEnabled = true
        self.delegate = delegate
        self.nameLabel.text = product.name
        self.numCell = num
        self.stockLabel.text = "Items: \(product.stock)"
        if let price = product.price{
            self.costLabel.text = "Cost:  \(price)$"
        }
        
    }
    
    // evento que ocurre cuando se da al boton de agregar al carrito
    @IBAction func moveToCart(){
        if let state = delegate?.buyProduct(num : numCell){
            if state == 0 { // si ya no quedan elementos al stock se bloquea el boton de comprar
                button.isEnabled = false
            } else {
                button.isEnabled = true
            }
          self.stockLabel.text = "Items: \(state)"
        }
    }
    

}
