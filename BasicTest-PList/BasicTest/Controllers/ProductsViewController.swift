//
//  ProductsViewController.swift
//  BasicTest
//
//  Created by Momentum Lab 1 on 2/7/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController {

    // id de la celda
    let idCell = "StockCell"
    
    //label externos de la vista
    @IBOutlet weak var stockLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    //puntero a la tabla
    @IBOutlet weak var stockTable: UITableView!
    
    // lista de productos
    var stockShop: [Product] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadBd() // se extrae la informacion del almacen
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // paso para cargar la vista de Onsale
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "onSaleId" {
            
            if let newView = segue.destination as? OnSaleViewController {
        
                var onSale:[(Product,Int)] = [] // se extraen los items que esten en el carrito
                for item in 0..<stockShop.count {
                    if stockShop[item].isCash(){
                    onSale.append((stockShop[item],item))
                    }
                }
                
                newView.onSaleShop = onSale //se le envia a la nueva vista junto al delegate
                newView.cellDelegate = self
                newView.viewDelegate = self
            }
        }
    }
    
}

//// se añaden las caracteristicas y funciones del tableviewdatasource
extension ProductsViewController: UITableViewDataSource {

    // numero de secciones
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // numero de celdas
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stockShop.count
    }
    
    // se selecciona el tipo de celdas
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: idCell, for: indexPath) as! StockTableViewCell
        
        // se actualiza el label de la celda, con la informacion del producto
        cell.cargarLabel(delegate: self, product: stockShop[indexPath.row],NumRow: indexPath.row)
        return cell
    }
}

extension ProductsViewController : StockCellProtocol {
    
    // evento que ocurre cuando se compra un producto desde el boton en la vista actual
    func buyProduct(num:Int) -> Int {
        let productTemporal = stockShop[num]
        let cash = productTemporal.addCash() // se agrega a caja el producto
        
        if cash {  // se actualiza los valores de la vista
            stockLabel.text = "\(Int(stockLabel.text!)!+1)"
            costLabel.text = "\(Float(costLabel.text!)!+productTemporal.price)"
        }
        
        return productTemporal.stock
    }
}

extension ProductsViewController : OnSaleCellProtocol {
    
    func returnProduct(num:Int) -> Int { // evento que ocurre cuando se retorna un producto
        let productTemporal = stockShop[num]
        let cash = productTemporal.addStock() // se añade al stock el elemento
        
        if cash { // se actualizan los label de la vista
            stockLabel.text = "\(Int(stockLabel.text!)!-1)"
            costLabel.text = "\(Float(costLabel.text!)!-productTemporal.price)"
        }
        stockTable.reloadData() // se actualiza la tabla
        return productTemporal.onSale
    }
    
    
}

extension ProductsViewController: onSaleViewProtocol {

    func buyAll(){ // evento que ocurre cuando se da al boton comprar en la vista onSale
        for item in stockShop {
            _ = item.sellStock() // se retornan todos los elementos al stock
        }
        stockTable.reloadData() //se actualizan las tablas
        stockLabel.text = "0"
        costLabel.text = "0"
    }
    
    func returnAll(row:Int){ // evento que ocurre cuando se desea devolver todos los elementos en caja del mismo producto
        let product = stockShop[row]
        stockLabel.text = "\(Int(stockLabel.text!)!-product.onSale)"
        costLabel.text = "\(Float(costLabel.text!)!-product.totalBuy())"
        _ = product.resetStock() // se devuelve el producto al stock
        stockTable.reloadData()
    }

}

